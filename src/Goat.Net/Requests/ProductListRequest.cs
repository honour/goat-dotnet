﻿using Goat.Net.Models.Enums;

namespace Goat.Net.Requests
{
	/// <summary>
	/// The data required to list a product on GOAT
	/// </summary>
	public class ProductListRequest
	{
		/// <summary>
		/// The price of the listing in the smallest denomination of US Dollar
		/// </summary>
		public long PriceCents { get; set; }
		/// <summary>
		/// The unit which <see cref="Size"/> is in. Likely "<c>us</c>"
		/// </summary>
		public string SizeUnit { get; set; }
		/// <summary>
		/// The ID of the product template which this listing is based on
		/// </summary>
		public long ProductTemplateId { get; set; }
		/// <summary>
		/// The size of the product
		/// </summary>
		public string Size { get; set; }
		/// <summary>
		/// The condition which the shoe is in
		/// </summary>
		public ShoeCondition ShoeCondition { get; set; }
		/// <summary>
		/// The condition which the shoe box is in
		/// </summary>
		public BoxCondition BoxCondition { get; set; }
		/// <summary>
		/// Whether or not the shoe has discoloration
		/// </summary>
		public bool HasDiscoloration { get; set; }
		/// <summary>
		/// Whether or not the shoe has tears
		/// </summary>
		public bool HasTears { get; set; }
		/// <summary>
		/// Whether or not the shoe has scuffs
		/// </summary>
		public bool HasScuffs { get; set; }
		/// <summary>
		/// Whether or not the shoe has missing insoles
		/// </summary>
		public bool HasMissingInsoles { get; set; }
		/// <summary>
		/// Whether or not the shoe has any defects
		/// </summary>
		public bool HasDefects { get; set; }
		/// <summary>
		/// Any other items which are missing/have issues
		/// </summary>
		public string MissingItems { get; set; }
	}
}