﻿using System;
using System.Collections.Generic;
using Goat.Net.Models;
using Newtonsoft.Json;

namespace Goat.Net.Responses
{
	/// <summary>
	/// The response provided by GOAT when a list of all currencies is requested
	/// </summary>
	public class CurrenciesResponse
	{
		/// <summary>
		/// The date and time at which the exchange rates provided will no longer be used
		/// </summary>
		[JsonProperty("rateExpirationDate")]
		public DateTimeOffset RateExpirationDate { get; set; }

		/// <summary>
		/// A list of <see cref="Currency"/> objects
		/// </summary>
		[JsonProperty("currencies")]
		public List<Currency> Currencies { get; set; }
	}
}