﻿using Newtonsoft.Json;

namespace Goat.Net.Responses
{
	/// <summary>
	/// A generic response type; to be used when only the success of the request needs to be known
	/// </summary>
	public class SuccessResponse
	{
		/// <summary>
		/// Whether or not the request succeeded
		/// </summary>
		[JsonProperty("success")]
		public bool Success { get; set; }
	}
}