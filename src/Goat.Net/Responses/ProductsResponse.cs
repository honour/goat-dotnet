﻿using System.Collections.Generic;
using Goat.Net.Models;
using Newtonsoft.Json;

namespace Goat.Net.Responses
{
	/// <summary>
	/// The response provided by GOAT when a list of products is requested
	/// </summary>
	public class ProductsResponse
	{
		/// <summary>
		/// A list of <see cref="Product"/> objects
		/// </summary>
		[JsonProperty("products")]
		public List<Product> Products { get; set; }
	}
}