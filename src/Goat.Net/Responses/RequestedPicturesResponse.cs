﻿using System.Collections.Generic;
using Goat.Net.Models;
using Newtonsoft.Json;

namespace Goat.Net.Responses
{
	/// <summary>
	/// The response provided by GOAT when a list of the pictures needed for a product is requested
	/// </summary>
	public class RequestedPicturesResponse
	{
		/// <summary>
		/// A list of <see cref="RequestedPicture"/> objects
		/// </summary>
		[JsonProperty("requestedPictures")]
		public List<RequestedPicture> RequestedPictures { get; set; }
	}
}