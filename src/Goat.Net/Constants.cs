﻿using System;

namespace Goat.Net
{
	internal static class Constants
	{
		private static readonly Uri BaseUri = new Uri("https://www.goat.com/api/v1/");
		public static readonly Uri UsersUri = new Uri(BaseUri, "users/");
		public static readonly Uri CurrenciesUri = new Uri(BaseUri, "currencies/");
		public static readonly Uri RegionPreferencesUri = new Uri(BaseUri, "region_preferences/");
		public static readonly Uri ProductTemplatesUri = new Uri(BaseUri, "product_templates/");
		public static readonly Uri ProductsUri = new Uri(BaseUri, "products/");
		public static readonly Uri OrdersUri = new Uri(BaseUri, "orders/");
	}
}