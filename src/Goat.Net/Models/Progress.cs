﻿using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object used to represent a progress stage for an order
	/// </summary>
	public class Progress
	{
		/// <summary>
		/// The position that this progress marker comes within the list
		/// </summary>
		[JsonProperty("order")]
		public long Order { get; set; }

		/// <summary>
		/// The key to identify this progress marker
		/// </summary>
		[JsonProperty("key")]
		public string Key { get; set; }

		/// <summary>
		/// The display name of this progress marker that is shown to the user
		/// </summary>
		[JsonProperty("displayName")]
		public string DisplayName { get; set; }

		/// <summary>
		/// Whether or not this stage will have a checkmark next to it within the GOAT app
		/// </summary>
		[JsonProperty("checked")]
		public bool Checked { get; set; }
	}
}