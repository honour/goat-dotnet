﻿using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object used to represent a user's statistics as a seller
	/// </summary>
	public class SellerCounts
	{
		/// <summary>
		/// The number of items the user is selling
		/// </summary>
		[JsonProperty("sellingCount")]
		public long SellingCount { get; set; }

		/// <summary>
		/// The number of listings which are consignments
		/// </summary>
		[JsonProperty("consignedListingsCount")]
		public long ConsignedListingsCount { get; set; }

		/// <summary>
		/// The number of listings which are marked as inactive
		/// </summary>
		[JsonProperty("inactiveListingsCount")]
		public long InactiveListingsCount { get; set; }

		/// <summary>
		/// The number of items which need to be confirmed
		/// </summary>
		[JsonProperty("needToConfirmCount")]
		public long NeedToConfirmCount { get; set; }

		/// <summary>
		/// The number of items which need to be shipped
		/// </summary>
		[JsonProperty("needToShipCount")]
		public long NeedToShipCount { get; set; }

		/// <summary>
		/// The number of items which have been shipped
		/// </summary>
		[JsonProperty("shippedCount")]
		public long ShippedCount { get; set; }
	}
}