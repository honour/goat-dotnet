﻿using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object used to represent a currency and its exchange rate
	/// </summary>
	public class Currency
	{
		/// <summary>
		/// The ISO code for this currency
		/// </summary>
		[JsonProperty("isoCode")]
		public string IsoCode { get; set; }

		/// <summary>
		/// The full name of the currency
		/// </summary>
		[JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>
		/// The currency symbol
		/// </summary>
		[JsonProperty("symbol")]
		public string Symbol { get; set; }

		/// <summary>
		/// A secondary symbol used to differentiate countries with the same currency symbol
		/// </summary>
		[JsonProperty("disambiguatedSymbol", NullValueHandling = NullValueHandling.Ignore)]
		public string DisambiguatedSymbol { get; set; }

		/// <summary>
		/// Whether the symbol comes before or after a monetary value
		/// </summary>
		[JsonProperty("symbolFirst")]
		public bool SymbolFirst { get; set; }

		/// <summary>
		/// The symbol used to seperate sets of 3 digits
		/// </summary>
		[JsonProperty("thousandsSeparator")]
		public string ThousandsSeparator { get; set; }

		/// <summary>
		/// The symbol used as the decimal point
		/// </summary>
		[JsonProperty("decimalMark")]
		public string DecimalMark { get; set; }

		/// <summary>
		/// Whether or not the currency has a cents unit
		/// </summary>
		[JsonProperty("hasNoCents")]
		public bool HasNoCents { get; set; }

		/// <summary>
		/// The current exchange rate between this currency and USD
		/// </summary>
		[JsonProperty("rate")]
		public double Rate { get; set; }
	}
}