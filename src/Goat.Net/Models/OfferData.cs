﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object to represent the offer data for a product
	/// </summary>
	public class OfferData
	{
		/// <summary>
		/// The cost of the highest bid in the smallest denomination of the user's local curreny
		/// </summary>
		[JsonProperty("highestOfferCents")]
		public Cents HighestOfferCents { get; set; }

		/// <summary>
		/// The cost of the lowest ask in the smallest denomination of the user's local curreny
		/// </summary>
		[JsonProperty("lowestPriceCents")]
		public Cents LowestPriceCents { get; set; }

		/// <summary>
		/// The minimum amount for an offer in the smallest denomination of the user's local curreny
		/// </summary>
		[JsonProperty("minimumOfferCents")]
		public Cents MinimumOfferCents { get; set; }

		/// <summary>
		/// The maximum amount for an offer in the smallest denomination of the user's local curreny
		/// </summary>
		[JsonProperty("maximumOfferCents")]
		public Cents MaximumOfferCents { get; set; }

		/// <summary>
		/// The increments in price in the smallest denomination of the user's local curreny
		/// </summary>
		[JsonProperty("priceIncrementCents")]
		public Cents PriceIncrementCents { get; set; }

		/// <summary>
		/// A list of <see cref="PriceDatum"/> objects providing a set of price data
		/// </summary>
		[JsonProperty("priceData")]
		public List<PriceDatum> PriceData { get; set; }
	}
	
	/// <summary>
	/// An object used to represent the smallest denomination of a particular currency
	/// </summary>
	public class Cents
	{
		/// <summary>
		/// The currency this object is for
		/// </summary>
		[JsonProperty("currency")]
		public string Currency { get; set; }

		/// <summary>
		/// The amount in the smallest denomination of the currency provided by <see cref="Currency"/>
		/// </summary>
		[JsonProperty("amount")]
		public long Amount { get; set; }

		/// <summary>
		/// The amount in the smallest denomination of US Dollar
		/// </summary>
		[JsonProperty("amountUsdCents")]
		public long AmountUsdCents { get; set; }
	}
	
	/// <summary>
	/// An object used to represent a single piece of price data
	/// </summary>
	public class PriceDatum
	{
		/// <summary>
		/// The date/time that the product was purchased at this price
		/// </summary>
		[JsonProperty("purchasedAt")]
		public DateTimeOffset PurchasedAt { get; set; }

		/// <summary>
		/// The US size of the product
		/// </summary>
		[JsonProperty("sizeUs")]
		public double SizeUs { get; set; }

		/// <summary>
		/// The price the product was bought at in the smallest denomination of <see cref="Cents.Currency"/>
		/// </summary>
		[JsonProperty("priceCents")]
		public Cents PriceCents { get; set; }
	}
}