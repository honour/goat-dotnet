﻿using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object used to represent a user's statistics as a buyer
	/// </summary>
	public class BuyerCounts
	{
		/// <summary>
		/// The number of items the user owns from GOAT
		/// </summary>
		[JsonProperty("ownCount")]
		public long OwnCount { get; set; }

		/// <summary>
		/// The number of items the user owns which are sneakers
		/// </summary>
		[JsonProperty("sneakersOwnCount")]
		public long SneakersOwnCount { get; set; }

		/// <summary>
		/// The number of items the user owns which are apparel
		/// </summary>
		[JsonProperty("apparelOwnCount")]
		public long ApparelOwnCount { get; set; }

		/// <summary>
		/// The number of items the user owns which are accessories
		/// </summary>
		[JsonProperty("accessoriesOwnCount")]
		public long AccessoriesOwnCount { get; set; }

		/// <summary>
		/// The number of items the user has listed as wanting
		/// </summary>
		[JsonProperty("wantCount")]
		public long WantCount { get; set; }

		/// <summary>
		/// The number of items the user has listed as wanting which are sneakers
		/// </summary>
		[JsonProperty("sneakersWantCount")]
		public long SneakersWantCount { get; set; }

		/// <summary>
		/// The number of items the user has listed as wanting which are apparel
		/// </summary>
		[JsonProperty("apparelWantCount")]
		public long ApparelWantCount { get; set; }

		/// <summary>
		/// The number of items the user has listed as wanting which are accessories
		/// </summary>
		[JsonProperty("accessoriesWantCount")]
		public long AccessoriesWantCount { get; set; }
	}
}