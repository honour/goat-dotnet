﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object used to represent a GOAT user
	/// </summary>
	public class User
    {
	    /// <summary>
	    /// The user's unique ID
	    /// </summary>
	    [JsonProperty("id")]
        public long Id { get; set; }

	    /// <summary>
	    /// The slug for accessing user resources
	    /// </summary>
        [JsonProperty("slug")]
        public string Slug { get; set; }

	    /// <summary>
	    /// The user's email address
	    /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

	    /// <summary>
	    /// The user's unique username
	    /// </summary>
        [JsonProperty("username")]
        public string Username { get; set; }

	    /// <summary>
	    /// The user's full name
	    /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

	    /// <summary>
	    /// The user's date of birth
	    /// </summary>
        [JsonProperty("dob")]
        public DateTimeOffset Dob { get; set; }

	    /// <summary>
	    /// The user's phone number
	    /// </summary>
        [JsonProperty("phone")]
        public string Phone { get; set; }

	    /// <summary>
	    /// The user's account type
	    /// </summary>
        [JsonProperty("accountType")]
        public string AccountType { get; set; }

	    /// <summary>
	    /// The user's account status
	    /// </summary>
        [JsonProperty("accountStatus")]
        public string AccountStatus { get; set; }
	    
	    /// <summary>
	    /// The user's account details
	    /// </summary>
        [JsonProperty("accountDetails")]
        public string AccountDetails { get; set; }

	    /// <summary>
	    /// The user's credit in the smallest denomination of US Dollar
	    /// </summary>
        [JsonProperty("creditCents")]
        public long CreditCents { get; set; }

	    /// <summary>
	    /// The last paid out amount in the smallest denomination of US Dollar
	    /// </summary>
        [JsonProperty("lastCashOutAmountCents")]
        public long LastCashOutAmountCents { get; set; }

	    /// <summary>
	    /// The unit used for shoe sizes. e.g. "us" or "uk"
	    /// </summary>
        [JsonProperty("sizeUnit")]
        public string SizeUnit { get; set; }

	    /// <summary>
	    /// Whether or not the user is interested in fashion
	    /// </summary>
        [JsonProperty("interestedInFashionResell")]
        public bool InterestedInFashionResell { get; set; }

	    /// <summary>
	    /// Whether or not the user has a proxy seller
	    /// </summary>
        [JsonProperty("hasProxySeller")]
        public bool HasProxySeller { get; set; }

	    /// <summary>
	    /// THe size unit after conversion
	    /// </summary>
        [JsonProperty("convertedSizeUnit")]
        public string ConvertedSizeUnit { get; set; }

	    /// <summary>
	    /// The user's status as a seller
	    /// </summary>
        [JsonProperty("sellerStatus")]
        public string SellerStatus { get; set; }

	    /// <summary>
	    /// The number of points the user has
	    /// </summary>
        [JsonProperty("points")]
        public long Points { get; set; }

	    /// <summary>
	    /// The number of wants the user has for sale
	    /// </summary>
        [JsonProperty("wantsForSale")]
        public long WantsForSale { get; set; }

	    /// <summary>
	    /// The user's seller score
	    /// </summary>
        [JsonProperty("sellerScore")]
        public long SellerScore { get; set; }

	    /// <summary>
	    /// The amount you recieve from sales after GOAT's percentage is taken
	    /// </summary>
        [JsonProperty("earningsRate")]
        public double EarningsRate { get; set; }

        /// <summary>
        /// The fee the seller will have to pay on the sale in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("sellerFeeCents")]
        public long SellerFeeCents { get; set; }

        /// <summary>
        /// The token for the user's shopping cart
        /// </summary>
        [JsonProperty("cartToken")]
        public string CartToken { get; set; }

        /// <summary>
        /// Whether of not the user's account is paused
        /// </summary>
        [JsonProperty("isPaused")]
        public bool IsPaused { get; set; }

        /// <summary>
        /// Whether or not the user's phone number has been verified
        /// </summary>
        [JsonProperty("phoneVerified")]
        public bool PhoneVerified { get; set; }

        /// <summary>
        /// Whether or not the user is a trusted seller
        /// </summary>
        [JsonProperty("trustedSeller")]
        public bool TrustedSeller { get; set; }

        /// <summary>
        /// Whether or not the user is a drop shipping partner
        /// </summary>
        [JsonProperty("dropShipPartner")]
        public bool DropShipPartner { get; set; }

        /// <summary>
        /// The date/time that the user accepted the seller agreement
        /// </summary>
        [JsonProperty("sellerAgreementAcceptedAt")]
        public DateTimeOffset SellerAgreementAcceptedAt { get; set; }

        /// <summary>
        /// The number of items the user owns from GOAT
        /// </summary>
        [JsonProperty("ownCount")]
        public long OwnCount { get; set; }

        /// <summary>
        /// The number of items the user has listed as wanting
        /// </summary>
        [JsonProperty("wantCount")]
        public long WantCount { get; set; }

        /// <summary>
        /// The number of items the user is selling
        /// </summary>
        [JsonProperty("sellingCount")]
        public long SellingCount { get; set; }

        /// <summary>
        /// The number of consigned listings the user has
        /// </summary>
        [JsonProperty("consignedListingsCount")]
        public long ConsignedListingsCount { get; set; }

        /// <summary>
        /// The number of orders the user has
        /// </summary>
        [JsonProperty("ordersCount")]
        public long OrdersCount { get; set; }

        /// <summary>
        /// The user's verified social media accounts
        /// </summary>
        [JsonProperty("verifiedSocials")]
        public VerifiedSocials VerifiedSocials { get; set; }

        /// <summary>
        /// The return address for the user
        /// </summary>
        [JsonProperty("returnAddress")]
        public Address ReturnAddress { get; set; }

        /// <summary>
        /// A list of application products
        /// </summary>
        [JsonProperty("applicationProducts")]
        public List<object> ApplicationProducts { get; set; }

        /// <summary>
        /// Whether or not the user is a guest
        /// </summary>
        [JsonProperty("isGuest")]
        public bool IsGuest { get; set; }

        /// <summary>
        /// The authentication token for user's acounts
        /// </summary>
        [JsonProperty("authToken")]
        public string AuthToken { get; set; }

        /// <summary>
        /// Whether or not the user has two factor authentication enabled
        /// </summary>
        [JsonProperty("twoFactorAuthenticated")]
        public bool TwoFactorAuthenticated { get; set; }
    }
	
	/// <summary>
	/// An object used to represent a user's address
	/// </summary>
	public class Address
	{
		/// <summary>
		/// The unique ID of this address
		/// </summary>
		[JsonProperty("id")]
		public long Id { get; set; }
		
		/// <summary>
		/// The name of the address
		/// </summary>
		[JsonProperty("name")]
		public string Name { get; set; }
		
		/// <summary>
		/// The first line of the address
		/// </summary>
		[JsonProperty("address1")]
		public string Address1 { get; set; }

		/// <summary>
		/// The second line of the address
		/// </summary>
		[JsonProperty("address2")]
		public string Address2 { get; set; }

		/// <summary>
		/// The city the address is located in
		/// </summary>
		[JsonProperty("city")]
		public string City { get; set; }

		/// <summary>
		/// The state the address is located in
		/// </summary>
		[JsonProperty("state")]
		public string State { get; set; }

		/// <summary>
		/// The code used to represent the <see cref="State"/>
		/// </summary>
		[JsonProperty("stateCode")]
		public string StateCode { get; set; }

		/// <summary>
		/// The postal code for this address
		/// </summary>
		[JsonProperty("postalCode")]
		public string PostalCode { get; set; }

		/// <summary>
		/// The country this is address is located in
		/// </summary>
		[JsonProperty("country")]
		public string Country { get; set; }

		/// <summary>
		/// THe code used to represent the <see cref="Country"/>
		/// </summary>
		[JsonProperty("countryCode")]
		public string CountryCode { get; set; }

		/// <summary>
		/// A phone number for this address
		/// </summary>
		[JsonProperty("phone")]
		public string Phone { get; set; }

		/// <summary>
		/// The type this address falls into
		/// </summary>
		[JsonProperty("addressType")]
		public string AddressType { get; set; }

		/// <summary>
		/// Whether or not UPS delivery is available at this address
		/// </summary>
		[JsonProperty("upsAvailable")]
		public bool UpsAvailable { get; set; }
	}

	/// <summary>
	/// An object used to represent verified social media accounts for a user
	/// </summary>
	public class VerifiedSocials
	{
	}
}