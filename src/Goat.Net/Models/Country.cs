﻿using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object used to represent a country
	/// </summary>
	public class Country
	{
		/// <summary>
		/// The ISO code for the country
		/// </summary>
		[JsonProperty("isoCode")]
		public string IsoCode { get; set; }

		/// <summary>
		/// The country name
		/// </summary>
		[JsonProperty("name")]
		public string Name { get; set; }

		/// <summary>
		/// The respective flag emoji for the country
		/// </summary>
		[JsonProperty("flag")]
		public string Flag { get; set; }

		/// <summary>
		/// The country's calling code
		/// </summary>
		[JsonProperty("countryCode")]
		public string CountryCode { get; set; }

		/// <summary>
		/// The currency used in the country
		/// </summary>
		[JsonProperty("currency")]
		public string Currency { get; set; }

		/// <summary>
		/// The size unit used in the country
		/// </summary>
		[JsonProperty("sizeUnit")]
		public string SizeUnit { get; set; }

		/// <summary>
		/// Whether or not GOAT ship to this country
		/// </summary>
		[JsonProperty("shipsTo")]
		public bool ShipsTo { get; set; }
	}
}