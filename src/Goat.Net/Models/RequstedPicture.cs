﻿using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// Contains information about a picture which has been requested for upload
	/// </summary>
	public class RequestedPicture
	{
		/// <summary>
		/// What type of image this refers to. See <see cref="Enums.ImageType"/>
		/// </summary>
		[JsonProperty("type")]
		public string Type { get; set; }

		/// <summary>
		/// Whether or not the picture is required for the item to be listed
		/// </summary>
		[JsonProperty("required")]
		public bool RequestedPictureRequired { get; set; }
	}
}