﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object used to represent a product listing on GOAT
	/// </summary>
	public class Product
    {
	    /// <summary>
	    /// The product ID
	    /// </summary>
        [JsonProperty("id")]
        public long Id { get; set; }
		
	    /// <summary>
	    /// The slug used for accessing resources related to this product
	    /// </summary>
        [JsonProperty("slug")]
        public string Slug { get; set; }

	    /// <summary>
	    /// The date/time that the product was created at
	    /// </summary>
        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

	    /// <summary>
	    /// The price of the product in the smallest denomination of US Dollar
	    /// </summary>
        [JsonProperty("priceCents")]
        public long PriceCents { get; set; }

	    /// <summary>
	    /// The size of the product (in US sizing)
	    /// </summary>
        [JsonProperty("size")]
        public double Size { get; set; }

	    /// <summary>
	    /// The condition of the product from 1 (best) to 3 (worst)
	    /// </summary>
        [JsonProperty("condition")]
        public long Condition { get; set; }

	    /// <summary>
	    /// The condition of the shoe
	    /// <remarks>Options are the same as in <see cref="Enums.ShoeCondition"/></remarks>
	    /// </summary>
        [JsonProperty("shoeCondition")]
        public string ShoeCondition { get; set; }

	    /// <summary>
	    /// The condition of the shoe box
	    /// <remarks>Options are the same as in <see cref="Enums.BoxCondition"/></remarks>
	    /// </summary>
        [JsonProperty("boxCondition")]
        public string BoxCondition { get; set; }

	    /// <summary>
	    /// Whether or not the shoe has tears
	    /// </summary>
        [JsonProperty("hasTears")]
        public bool HasTears { get; set; }

	    /// <summary>
	    /// Whether or not the shoe has an odor
	    /// </summary>
        [JsonProperty("hasOdor")]
        public bool HasOdor { get; set; }

	    /// <summary>
	    /// Whether or not the shoe has any discoloration
	    /// </summary>
        [JsonProperty("hasDiscoloration")]
        public bool HasDiscoloration { get; set; }

	    /// <summary>
	    /// Whether or not the shoe has any defects
	    /// </summary>
        [JsonProperty("hasDefects")]
        public bool HasDefects { get; set; }

	    /// <summary>
	    /// Whether or not the shoe has scuffs
	    /// </summary>
        [JsonProperty("hasScuffs")]
        public bool HasScuffs { get; set; }

	    /// <summary>
	    /// Whether or not the shoe has missing insoles
	    /// </summary>
        [JsonProperty("hasMissingInsoles")]
        public bool HasMissingInsoles { get; set; }

	    /// <summary>
	    /// How many items are in the listing
	    /// </summary>
        [JsonProperty("quantity")]
        public long Quantity { get; set; }

	    /// <summary>
	    /// Whether or not the listing message has been read
	    /// </summary>
        [JsonProperty("messageRead")]
        public bool MessageRead { get; set; }

        /// <summary>
        /// The size option for this product
        /// </summary>
        [JsonProperty("sizeOption")]
        public SizeOption SizeOption { get; set; }

        /// <summary>
        /// Whether or not the product is replaceable
        /// </summary>
        [JsonProperty("isFungible")]
        public bool IsFungible { get; set; }

        /// <summary>
        /// The price of the item in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedPriceCents")]
        public Cents LocalizedPriceCents { get; set; }

        /// <summary>
        /// The status of the product's sale
        /// </summary>
        [JsonProperty("saleStatus")]
        public string SaleStatus { get; set; }

        /// <summary>
        /// Whether or not the product can be instantly shipped
        /// </summary>
        [JsonProperty("instantShippable")]
        public bool InstantShippable { get; set; }

        /// <summary>
        /// Whether or not the product is GOAT clean
        /// </summary>
        [JsonProperty("isGoatClean")]
        public bool IsGoatClean { get; set; }

        /// <summary>
        /// Whether or not the product is consigned
        /// </summary>
        [JsonProperty("consigned")]
        public bool Consigned { get; set; }

        /// <summary>
        /// The product listing status
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// The URL of the main product picture
        /// </summary>
        [JsonProperty("mainPictureUrl")]
        public Uri MainPictureUrl { get; set; }

        /// <summary>
        /// The cost of the highest bid in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedProductHighestOfferCents")]
        public Cents LocalizedProductHighestOfferCents { get; set; }

        /// <summary>
        /// The URL of the product outer picture
        /// </summary>
        [JsonProperty("outerPictureUrl")]
        public Uri OuterPictureUrl { get; set; }

        /// <summary>
        /// The URL of the product outer thumbnail picture
        /// </summary>
        [JsonProperty("thumbOuterPictureUrl")]
        public Uri ThumbOuterPictureUrl { get; set; }

        /// <summary>
        /// The short URL for the product page
        /// </summary>
        [JsonProperty("shortUrl")]
        public Uri ShortUrl { get; set; }

        /// <summary>
        /// A list of product images
        /// </summary>
        [JsonProperty("productImages")]
        public List<ProductImage> ProductImages { get; set; }

        /// <summary>
        /// The cost of the lowest ask in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("lowestPriceCents")]
        public long LowestPriceCents { get; set; }

        /// <summary>
        /// The cost of the lowest ask in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedLowestPriceCents")]
        public Cents LocalizedLowestPriceCents { get; set; }

        /// <summary>
        /// The cost of the previous lowest ask in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("previousLowestPriceCents")]
        public long PreviousLowestPriceCents { get; set; }

        /// <summary>
        /// The cost of the previous lowest ask in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedPreviousLowestPriceCents")]
        public Cents LocalizedPreviousLowestPriceCents { get; set; }

        /// <summary>
        /// The cost of the highest bid in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("highestOfferCents")]
        public long HighestOfferCents { get; set; }

        /// <summary>
        /// The cost of the highest bid in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedHighestOfferCents")]
        public Cents LocalizedHighestOfferCents { get; set; }

        /// <summary>
        /// The cost of the previous highest bid in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("previousHighestOfferCents")]
        public long PreviousHighestOfferCents { get; set; }

        /// <summary>
        /// The cost of the previous highest bid in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedPreviousHighestOfferCents")]
        public Cents LocalizedPreviousHighestOfferCents { get; set; }

        /// <summary>
        /// The price the product was last sold at in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("lastSoldPriceCents")]
        public long LastSoldPriceCents { get; set; }

        /// <summary>
        /// The price the product was previously sold at in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("previousSoldPriceCents")]
        public long PreviousSoldPriceCents { get; set; }

        /// <summary>
        /// The price the product was last sold at in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedLastSoldPriceCents")]
        public Cents LocalizedLastSoldPriceCents { get; set; }

        /// <summary>
        /// The price the product was previously sold at in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedPreviousSoldPriceCents")]
        public Cents LocalizedPreviousSoldPriceCents { get; set; }

        /// <summary>
        /// The product template that this product listing is based on
        /// </summary>
        [JsonProperty("productTemplate")]
        public ProductTemplate ProductTemplate { get; set; }
    }
	
	/// <summary>
	/// An object used to represent an image of a product
	/// </summary>
	public class ProductImage
	{
		/// <summary>
		/// The URL to access the image
		/// </summary>
		[JsonProperty("url")]
		public Uri Url { get; set; }

		/// <summary>
		/// The date/time that the image was taken at
		/// </summary>
		[JsonProperty("createdAt")]
		public DateTimeOffset CreatedAt { get; set; }

		/// <summary>
		/// The type of picture it is
		/// <remarks>Options are the same as in <see cref="Enums.ImageType"/></remarks>
		/// </summary>
		[JsonProperty("pictureType")]
		public string PictureType { get; set; }
	}
}