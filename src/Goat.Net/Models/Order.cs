﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object used to represent an order placed on GOAT
	/// </summary>
	public class Order
    {
	    /// <summary>
	    /// The unique ID for the order
	    /// </summary>
        [JsonProperty("id")]
        public long Id { get; set; }

	    /// <summary>
	    /// The ID of the product this order is for
	    /// </summary>
        [JsonProperty("productId")]
        public long ProductId { get; set; }

	    /// <summary>
	    /// The order number
	    /// </summary>
        [JsonProperty("number")]
        public long Number { get; set; }

	    /// <summary>
	    /// The cost of shipping in the smallest denomination of US Dollar
	    /// </summary>
        [JsonProperty("shippingCents")]
        public long ShippingCents { get; set; }

	    /// <summary>
	    /// The level of service for shipping this item
	    /// </summary>
        [JsonProperty("shippingServiceLevel")]
        public string ShippingServiceLevel { get; set; }

	    /// <summary>
	    /// The order status
	    /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

	    /// <summary>
	    /// The date/time that the order was last updated
	    /// </summary>
        [JsonProperty("updatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }

	    /// <summary>
	    /// Whether or not the product requires additional verification
	    /// </summary>
        [JsonProperty("addVerification")]
        public bool AddVerification { get; set; }

        /// <summary>
        /// The fee to verify the product in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("verificationFeeCents")]
        public long VerificationFeeCents { get; set; }

        /// <summary>
        /// The cost of any additional tax in the smallest denomation of US Dollar
        /// </summary>
        [JsonProperty("additionalTaxCents")]
        public long AdditionalTaxCents { get; set; }

        /// <summary>
        /// The cost of shipping in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedShippingCents")]
        public Cents LocalizedShippingCents { get; set; }

        /// <summary>
        /// The amount of available credit from this order in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedAvailableCreditCents")]
        public Cents LocalizedAvailableCreditCents { get; set; }

        /// <summary>
        /// The amount of credit from this order in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedCreditCents")]
        public Cents LocalizedCreditCents { get; set; }

        /// <summary>
        /// The amount of credit to use for this order in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedCreditsToUseCents")]
        public Cents LocalizedCreditsToUseCents { get; set; }

        /// <summary>
        /// The amount of credit used for this order in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedCreditsUsedCents")]
        public Cents LocalizedCreditsUsedCents { get; set; }

        /// <summary>
        /// The amount made by the seller in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedSellerAmountMadeCents")]
        public Cents LocalizedSellerAmountMadeCents { get; set; }

        /// <summary>
        /// The price of the order in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedPriceCents")]
        public Cents LocalizedPriceCents { get; set; }

        /// <summary>
        /// The listing price of the order in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedListPriceCents")]
        public Cents LocalizedListPriceCents { get; set; }

        /// <summary>
        /// The value of the promo code used in the smallest denominiation of the user's local currency (if applicable)
        /// </summary>
        [JsonProperty("localizedPromoCodeValueCents")]
        public Cents LocalizedPromoCodeValueCents { get; set; }

        /// <summary>
        /// The final order price in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedFinalPriceCents")]
        public Cents LocalizedFinalPriceCents { get; set; }

        /// <summary>
        /// The markup charged for instant shipping in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedInstantShipMarkupFeeCents")]
        public Cents LocalizedInstantShipMarkupFeeCents { get; set; }

        /// <summary>
        /// The amount of tax to be paid for the order in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedTaxCents")]
        public Cents LocalizedTaxCents { get; set; }

        /// <summary>
        /// The processing fee for the order in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedProcessingFeeCents")]
        public Cents LocalizedProcessingFeeCents { get; set; }

        /// <summary>
        /// The ID of the user who placed this order
        /// </summary>
        [JsonProperty("userId")]
        public long UserId { get; set; }

        /// <summary>
        /// The ID of the address which this order will be sent to
        /// </summary>
        [JsonProperty("addressId")]
        public long AddressId { get; set; }

        /// <summary>
        /// The ID of the billing info used to place this order
        /// </summary>
        [JsonProperty("billingInfoId")]
        public long BillingInfoId { get; set; }

        /// <summary>
        /// The ID of the want that this order is related to
        /// </summary>
        [JsonProperty("wantId")]
        public long WantId { get; set; }

        /// <summary>
        /// The GOAT credits to use on the order in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("creditsToUseCents")]
        public long CreditsToUseCents { get; set; }

        /// <summary>
        /// The list of options for <see cref="ShippingServiceLevel"/>
        /// </summary>
        [JsonProperty("shippingServiceLevelOptions")]
        public List<string> ShippingServiceLevelOptions { get; set; }

        /// <summary>
        /// The date/time that the order was purchased at
        /// </summary>
        [JsonProperty("purchasedAt")]
        public DateTimeOffset PurchasedAt { get; set; }

        /// <summary>
        /// The price of the order in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("priceCents")]
        public long PriceCents { get; set; }

        /// <summary>
        /// The unique ID of the promo code
        /// </summary>
        [JsonProperty("promoCodeLedgerId")]
        public long PromoCodeLedgerId { get; set; }

        /// <summary>
        /// The value of the promo code used in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("promoCodeValueCents")]
        public long PromoCodeValueCents { get; set; }

        /// <summary>
        /// The amount made by the seller in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("sellerAmountMadeCents")]
        public long SellerAmountMadeCents { get; set; }

        /// <summary>
        /// The sequence of progress for the order
        /// </summary>
        [JsonProperty("progressSequence")]
        public string ProgressSequence { get; set; }

        /// <summary>
        /// The amount of credit used for this order in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("creditsUsedCents")]
        public long CreditsUsedCents { get; set; }

        /// <summary>
        /// The final price of the order in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("finalPriceCents")]
        public long FinalPriceCents { get; set; }

        /// <summary>
        /// Whether or not the order is in it's final state
        /// </summary>
        [JsonProperty("endState")]
        public bool EndState { get; set; }

        /// <summary>
        /// The product that this order is for
        /// </summary>
        [JsonProperty("product")]
        public Product Product { get; set; }

        /// <summary>
        /// The title for the seller
        /// </summary>
        [JsonProperty("sellerTitle")]
        public string SellerTitle { get; set; }

        /// <summary>
        /// The description for the seller
        /// </summary>
        [JsonProperty("sellerDescription")]
        public string SellerDescription { get; set; }

        /// <summary>
        /// A list of actions the seller can perform
        /// </summary>
        [JsonProperty("sellerActions")]
        public List<Action> SellerActions { get; set; }

        /// <summary>
        /// The title for the buyer
        /// </summary>
        [JsonProperty("buyerTitle")]
        public string BuyerTitle { get; set; }

        /// <summary>
        /// The description for the buyer
        /// </summary>
        [JsonProperty("buyerDescription")]
        public string BuyerDescription { get; set; }

        /// <summary>
        /// A list of actions the buyer can perform
        /// </summary>
        [JsonProperty("buyerActions")]
        public List<Action> BuyerActions { get; set; }

        /// <summary>
        /// A list of pictures the buyer has provided for order issues
        /// </summary>
        [JsonProperty("buyerIssuePictures")]
        public List<object> BuyerIssuePictures { get; set; }

        /// <summary>
        /// The discount percentage the buyer has been given
        /// </summary>
        [JsonProperty("buyerDiscountPercentage")]
        public long BuyerDiscountPercentage { get; set; }

        /// <summary>
        /// The formatted amount for the buyer discount
        /// </summary>
        [JsonProperty("buyerDiscountAmountFormatted")]
        public string BuyerDiscountAmountFormatted { get; set; }

        /// <summary>
        /// Whether or not the sale is final
        /// </summary>
        [JsonProperty("finalSale")]
        public bool FinalSale { get; set; }
    }
	
	/// <summary>
	/// An object used to represent an action which can be performed by a buyer or seller
	/// </summary>
	public class Action
	{
		/// <summary>
		/// The title of the action button
		/// </summary>
		[JsonProperty("buttonTitle")]
		public string ButtonTitle { get; set; }

		/// <summary>
		/// The action the button will perform
		/// </summary>
		[JsonProperty("buttonAction")]
		public string ButtonAction { get; set; }

		/// <summary>
		/// The type of button
		/// </summary>
		[JsonProperty("buttonType")]
		public string ButtonType { get; set; }

		/// <summary>
		/// The confirmation title
		/// </summary>
		[JsonProperty("confirmTitle")]
		public string ConfirmTitle { get; set; }

		/// <summary>
		/// The confirmation message
		/// </summary>
		[JsonProperty("confirmMessage")]
		public string ConfirmMessage { get; set; }
	}
}