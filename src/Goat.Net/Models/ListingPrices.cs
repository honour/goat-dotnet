﻿using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object used to represent the prices that a product is listed at
	/// </summary>
	public class ListingPrices
	{
		/// <summary>
		/// The minimum listing price in the smallest denomination of US Dollar
		/// </summary>
		[JsonProperty("minListingPriceCents")]
		public long MinListingPriceCents { get; set; }

		/// <summary>
		/// The maximum listing price in the smallest denomination of US Dollar
		/// </summary>
		[JsonProperty("maxListingPriceCents")]
		public long MaxListingPriceCents { get; set; }

		/// <summary>
		/// The rate GOAT takes as a commission
		/// </summary>
		[JsonProperty("commissionRate")]
		public double CommissionRate { get; set; }

		/// <summary>
		/// The fee the seller will have to pay on the sale in the smallest denomination of US Dollar
		/// </summary>
		[JsonProperty("sellerFeeCents")]
		public long SellerFeeCents { get; set; }
	}
}