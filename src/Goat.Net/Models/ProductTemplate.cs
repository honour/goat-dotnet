﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object used to represent a product template on GOAT
	/// </summary>
	public class ProductTemplate
    {
	    /// <summary>
	    /// The name of the brand the shoe is from
	    /// </summary>
        [JsonProperty("brandName")]
        public string BrandName { get; set; }

	    /// <summary>
	    /// Instructions on how to take care of the product
	    /// </summary>
        [JsonProperty("careInstructions")]
        public string CareInstructions { get; set; }

	    /// <summary>
	    /// The color of the product
	    /// </summary>
        [JsonProperty("color")]
        public string Color { get; set; }

	    /// <summary>
	    /// The materials the product is composed from
	    /// </summary>
        [JsonProperty("composition")]
        public string Composition { get; set; }

	    /// <summary>
	    /// The designer of the product
	    /// </summary>
        [JsonProperty("designer")]
        public string Designer { get; set; }

	    /// <summary>
	    /// Extra details about the product
	    /// </summary>
        [JsonProperty("details")]
        public string Details { get; set; }

        /// <summary>
        /// What type of fit the shoe has
        /// </summary>
        [JsonProperty("fit")]
        public string Fit { get; set; }

        /// <summary>
        /// A list of genders that the product is designed for
        /// </summary>
        [JsonProperty("gender")]
        public List<string> Gender { get; set; }

        /// <summary>
        /// The ID of the product template
        /// </summary>
        [JsonProperty("id")]
        public long Id { get; set; }
        
        /// <summary>
        /// A value representing whether the product images were taken by GOAT
        /// </summary>
        [JsonProperty("internalShot")]
        public string InternalShot { get; set; }

        /// <summary>
        /// The maximum amount for an offer in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("maximumOfferCents")]
        public long MaximumOfferCents { get; set; }

        /// <summary>
        /// The material the midsole is made from
        /// </summary>
        [JsonProperty("midsole")]
        public string Midsole { get; set; }

        /// <summary>
        /// The minimum amount for an offer in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("minimumOfferCents")]
        public long MinimumOfferCents { get; set; }

        /// <summary>
        /// The size of the model that product images are taken on (if applicable)
        /// </summary>
        [JsonProperty("modelSizing")]
        public string ModelSizing { get; set; }

        /// <summary>
        /// The full name of the product
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// The product's nickname (if applicable)
        /// </summary>
        [JsonProperty("nickname")]
        public string Nickname { get; set; }

        /// <summary>
        /// The category that the product falls into
        /// </summary>
        [JsonProperty("productCategory")]
        public string ProductCategory { get; set; }

        /// <summary>
        /// A more specific type that the product falls into
        /// </summary>
        [JsonProperty("productType")]
        public string ProductType { get; set; }

        /// <summary>
        /// The date/time that the product was initially released
        /// </summary>
        [JsonProperty("releaseDate")]
        public DateTimeOffset ReleaseDate { get; set; }

        /// <summary>
        /// The silhouette that this shoe is based on
        /// </summary>
        [JsonProperty("silhouette")]
        public string Silhouette { get; set; }

        /// <summary>
        /// The brand that the size chart is based on
        /// </summary>
        [JsonProperty("sizeBrand")]
        public string SizeBrand { get; set; }

        /// <summary>
        /// The range of sizes available
        /// </summary>
        [JsonProperty("sizeRange")]
        public List<double> SizeRange { get; set; }

        /// <summary>
        /// The types of sizes available (e.g Numeric)
        /// </summary>
        [JsonProperty("sizeType")]
        public string SizeType { get; set; }

        /// <summary>
        /// The unit used to represent the sizes
        /// </summary>
        [JsonProperty("sizeUnit")]
        public string SizeUnit { get; set; }

        /// <summary>
        /// The SKU used for this shoe
        /// </summary>
        [JsonProperty("sku")]
        public string Sku { get; set; }

        /// <summary>
        /// The slug to access other resources for this product template
        /// </summary>
        [JsonProperty("slug")]
        public string Slug { get; set; }

        /// <summary>
        /// The special display price in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("specialDisplayPriceCents")]
        public long SpecialDisplayPriceCents { get; set; }

        /// <summary>
        /// The special type of the product
        /// </summary>
        [JsonProperty("specialType")]
        public string SpecialType { get; set; }

        /// <summary>
        /// The status of this product template
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// The material that the upper of the shoe is made from
        /// </summary>
        [JsonProperty("upperMaterial")]
        public string UpperMaterial { get; set; }

        /// <summary>
        /// A list of sizes that the shoe is available as new in
        /// </summary>
        [JsonProperty("availableSizesNew")]
        public List<double> AvailableSizesNew { get; set; }

        /// <summary>
        /// A list of sizes that the shoe is available as new in
        /// </summary>
        [JsonProperty("availableSizesNewV2")]
        public List<double> AvailableSizesNewV2 { get; set; }

        /// <summary>
        /// A list of sizes that the shoe is available as new in, but containing defects
        /// </summary>
        [JsonProperty("availableSizesNewWithDefects")]
        public List<double> AvailableSizesNewWithDefects { get; set; }

        /// <summary>
        /// A list of sizes that the shoe is available as used in
        /// </summary>
        [JsonProperty("availableSizesUsed")]
        public List<double> AvailableSizesUsed { get; set; }

        /// <summary>
        /// The lowest price the shoe is available as new in, in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("newLowestPriceCents")]
        public long NewLowestPriceCents { get; set; }

        /// <summary>
        /// The lowest price the shoe is available as used in, in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("usedLowestPriceCents")]
        public long UsedLowestPriceCents { get; set; }

        /// <summary>
        /// A list that explains the product's taxonomy
        /// </summary>
        [JsonProperty("productTaxonomy")]
        public List<object> ProductTaxonomy { get; set; }

        /// <summary>
        /// The cost of the lowest ask in the smallest denomination of US Dollar
        /// </summary>
        [JsonProperty("lowestPriceCents")]
        public long LowestPriceCents { get; set; }

        /// <summary>
        /// The special display price in the smallest denomination of the user's local currency
        /// </summary>
        [JsonProperty("localizedSpecialDisplayPriceCents")]
        public Cents LocalizedSpecialDisplayPriceCents { get; set; }

        /// <summary>
        /// A list of categories this product falls into
        /// </summary>
        [JsonProperty("category")]
        public List<string> Category { get; set; }

        /// <summary>
        /// The number of microposts for this product
        /// </summary>
        [JsonProperty("micropostsCount")]
        public long MicropostsCount { get; set; }

        /// <summary>
        /// The number of times this product is being sold
        /// </summary>
        [JsonProperty("sellingCount")]
        public long SellingCount { get; set; }

        /// <summary>
        /// The number of tiems this product is being sold as used
        /// </summary>
        [JsonProperty("usedForSaleCount")]
        public long UsedForSaleCount { get; set; }

        /// <summary>
        /// The number of tiems this product is being sold with defects
        /// </summary>
        [JsonProperty("withDefectForSaleCount")]
        public long WithDefectForSaleCount { get; set; }

        /// <summary>
        /// Whether or not this product can be marked as wanted
        /// </summary>
        [JsonProperty("isWantable")]
        public bool IsWantable { get; set; }

        /// <summary>
        /// Whether or not this product can be owned
        /// </summary>
        [JsonProperty("isOwnable")]
        public bool IsOwnable { get; set; }

        /// <summary>
        /// Whether or not the product can be resold
        /// </summary>
        [JsonProperty("isResellable")]
        public bool IsResellable { get; set; }

        /// <summary>
        /// Whether or not this is a fashion product
        /// </summary>
        [JsonProperty("isFashionProduct")]
        public bool IsFashionProduct { get; set; }

        /// <summary>
        /// Whether or not this is a raffle product
        /// </summary>
        [JsonProperty("isRaffleProduct")]
        public bool IsRaffleProduct { get; set; }

        /// <summary>
        /// The single gender for this product
        /// </summary>
        [JsonProperty("singleGender")]
        public string SingleGender { get; set; }

        /// <summary>
        /// The URL for the product picture
        /// </summary>
        [JsonProperty("pictureUrl")]
        public Uri PictureUrl { get; set; }

        /// <summary>
        /// The URL for the glow picture of the product
        /// </summary>
        [JsonProperty("mainGlowPictureUrl")]
        public Uri MainGlowPictureUrl { get; set; }

        /// <summary>
        /// The URL for the main product picture
        /// </summary>
        [JsonProperty("mainPictureUrl")]
        public Uri MainPictureUrl { get; set; }

        /// <summary>
        /// The glow picture of the product for the grid
        /// </summary>
        [JsonProperty("gridGlowPictureUrl")]
        public Uri GridGlowPictureUrl { get; set; }

        /// <summary>
        /// The picture of the prodduct for the grid
        /// </summary>
        [JsonProperty("gridPictureUrl")]
        public Uri GridPictureUrl { get; set; }

        /// <summary>
        /// A list of the size options available
        /// </summary>
        [JsonProperty("sizeOptions")]
        public List<SizeOption> SizeOptions { get; set; }

        /// <summary>
        /// Whether or not the logged-in user wants this product
        /// </summary>
        [JsonProperty("userWant")]
        public bool UserWant { get; set; }

        /// <summary>
        /// Whether or not the logged-in user owns this product
        /// </summary>
        [JsonProperty("userOwn")]
        public bool UserOwn { get; set; }

        /// <summary>
        /// A list of external pictures for this product template
        /// </summary>
        [JsonProperty("productTemplateExternalPictures")]
        public List<ProductTemplateExternalPicture> ProductTemplateExternalPictures { get; set; }
    }
	
	/// <summary>
	/// An object used to represent an external product template picture
	/// </summary>
	public class ProductTemplateExternalPicture
	{
		/// <summary>
		/// The URL of the main picture
		/// </summary>
		[JsonProperty("mainPictureUrl")]
		public Uri MainPictureUrl { get; set; }

		/// <summary>
		/// The URL of the grid picture
		/// </summary>
		[JsonProperty("gridPictureUrl")]
		public Uri GridPictureUrl { get; set; }

		/// <summary>
		/// The dominant colour of the picture
		/// </summary>
		[JsonProperty("dominantColor")]
		public string DominantColor { get; set; }

		/// <summary>
		/// The source URL for the picture(s)
		/// </summary>
		[JsonProperty("sourceUrl")]
		public Uri SourceUrl { get; set; }

		/// <summary>
		/// The attribution URL for the picture(s)
		/// </summary>
		[JsonProperty("attributionUrl")]
		public string AttributionUrl { get; set; }

		/// <summary>
		/// The aspect ratio of the picture
		/// </summary>
		[JsonProperty("aspect")]
		public double Aspect { get; set; }

		/// <summary>
		/// The order the picture(s) came from
		/// </summary>
		[JsonProperty("order")]
		public long Order { get; set; }
	}
	
	/// <summary>
	/// An object used to represent a size option for a product template
	/// </summary>
	public class SizeOption
	{
		/// <summary>
		/// The display value for the size
		/// </summary>
		[JsonProperty("presentation")]
		public string Presentation { get; set; }

		/// <summary>
		/// The numerical value for the size
		/// </summary>
		[JsonProperty("value")]
		public double Value { get; set; }
	}
}