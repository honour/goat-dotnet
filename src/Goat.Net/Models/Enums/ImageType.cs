﻿namespace Goat.Net.Models.Enums
{
	/// <summary>
	/// The possible options for requested image type
	/// </summary>
	public enum ImageType
	{
		/// <summary>
		/// The outside of the shoe
		/// </summary>
		[StringValue("outerPhotoData")] Outer,
		/// <summary>
		/// The inside of the shoe
		/// </summary>
		[StringValue("innerPhotoData")] Inner,
		/// <summary>
		/// The sole of the shoe
		/// </summary>
		[StringValue("solePhotoData")] Sole,
		/// <summary>
		/// The top of the shoe
		/// </summary>
		[StringValue("topPhotoData")] Top,
		/// <summary>
		/// The back of the shoe
		/// </summary>
		[StringValue("backPhotoData")] Back,
		/// <summary>
		/// The tag inside the shoe
		/// </summary>
		[StringValue("shoeTagPhotoData")] ShoeTag,
		/// <summary>
		/// The shoe box
		/// </summary>
		[StringValue("shoeBoxPhotoData")] ShoeBox,
		/// <summary>
		/// First extra picture
		/// </summary>
		[StringValue("extra1PhotoData")] Extra1,
		/// <summary>
		/// Second extra picture
		/// </summary>
		[StringValue("extra2PhotoData")] Extra2,
		/// <summary>
		/// Third extra picture
		/// </summary>
		[StringValue("extra3PhotoData")] Extra3
	}
}