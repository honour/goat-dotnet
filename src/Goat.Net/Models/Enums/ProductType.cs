﻿namespace Goat.Net.Models.Enums
{
	/// <summary>
	/// The possible options for filtering products by type
	/// </summary>
	public enum ProductType
	{
		/// <summary>
		/// The product is being sold
		/// </summary>
		[StringValue("selling")] Selling,
		/// <summary>
		/// The product is being consigned
		/// </summary>
		[StringValue("consigned")] Consigned,
		/// <summary>
		/// The product type is pending
		/// </summary>
		[StringValue("pending")] Pending
	}
}