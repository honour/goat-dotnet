﻿namespace Goat.Net.Models.Enums
{
	/// <summary>
	/// The possible options for filtering orders by status
	/// </summary>
	public enum OrderStatus
	{
		/// <summary>
		/// The order is being packaged by the seller
		/// </summary>
		[StringValue("seller_packaging")] SellerPackaging,
		/// <summary>
		/// The order is yet to be confirmed by the seller
		/// </summary>
		[StringValue("seller_confirm")] SellerConfirm
	}
}