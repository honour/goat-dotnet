﻿using System;

namespace Goat.Net.Models.Enums
{
	internal class StringValueAttribute : Attribute
	{
		public string StringValue { get; }

		public StringValueAttribute(string stringValue)
		{
			StringValue = stringValue;
		}
	}
	
	internal static class EnumExtensions
	{
		internal static string GetStringValue(this Enum e)
		{
			var type = e.GetType();
			var fieldInfo = type.GetField(e.ToString());

			return
				fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false) is StringValueAttribute[] attribs &&
				attribs.Length > 0
					? attribs[0].StringValue
					: null;
		}
	}
}