﻿namespace Goat.Net.Models.Enums
{
	/// <summary>
	/// The possible options for shoe condition
	/// </summary>
	public enum ShoeCondition
	{
		/// <summary>
		/// The shoe is new (deadstock)
		/// </summary>
		[StringValue("new")] New,
		/// <summary>
		/// The shoe has been used
		/// </summary>
		[StringValue("used")] Used
	}
}