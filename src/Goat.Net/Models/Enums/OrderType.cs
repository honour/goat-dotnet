﻿namespace Goat.Net.Models.Enums
{
	/// <summary>
	/// The possible options for filtering orders by type
	/// </summary>
	public enum OrderType
	{
		/// <summary>
		/// The order needs to be confirmed
		/// </summary>
		[StringValue("need_to_confirm")] NeedToConfirm,
		/// <summary>
		/// The order needs to be shipped
		/// </summary>
		[StringValue("need_to_ship")] NeedToShip,
		/// <summary>
		/// The seller has shipped the order
		/// </summary>
		[StringValue("seller_shipped")] SellerShipped,
		/// <summary>
		/// The order's a user is selling
		/// </summary>
		[StringValue("sell")] Sell,
		/// <summary>
		/// The order's a user is buying
		/// </summary>
		[StringValue("buy")] Buy
	}
}