﻿namespace Goat.Net.Models.Enums
{
	/// <summary>
	/// The possible options for shoe box condition
	/// </summary>
	public enum BoxCondition
	{
		/// <summary>
		/// The shoe box is badly damaged
		/// </summary>
		[StringValue("badly_damaged")] BadlyDamaged,
		/// <summary>
		/// The shoe box is intact, but missing the lid
		/// </summary>
		[StringValue("missing_lid")] MissingLid,
		/// <summary>
		/// There is no original box
		/// </summary>
		[StringValue("no_original_box")] NoOriginalBox,
		/// <summary>
		/// The shoe box is in good condition
		/// </summary>
		[StringValue("good_condition")] GoodCondition
	}
}