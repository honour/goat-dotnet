﻿using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// An object to store a user's cash out information
	/// </summary>
	public class CashOut
	{
		/// <summary>
		/// The amount available to cash out in the smallest denomination of US Dollar
		/// </summary>
		[JsonProperty("cashAvailableCents")]
		public long CashAvailableCents { get; set; }

		/// <summary>
		/// The amount pending cash out in the smallest denomination of US Dollar
		/// </summary>
		[JsonProperty("cashPendingCents")]
		public long CashPendingCents { get; set; }

		/// <summary>
		/// The amount sent last time the user cashed out in the smallest denomination of US Dollar
		/// </summary>
		[JsonProperty("lastCashOutAmountCents")]
		public long LastCashOutAmountCents { get; set; }
		
		/// <summary>
		/// The total amount of credit that the user has in the smallest denomination of US Dollar
		/// </summary>
		[JsonProperty("totalCreditCents")]
		public long TotalCreditCents { get; set; }
	}
}