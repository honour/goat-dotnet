﻿using Newtonsoft.Json;

namespace Goat.Net.Models
{
	/// <summary>
	/// A region object
	/// </summary>
	public class Region
	{
		/// <summary>
		/// The <see cref="Models.Country"/> object relating to this region
		/// </summary>
		[JsonProperty("country")]
		public Country Country { get; set; }
	}
}