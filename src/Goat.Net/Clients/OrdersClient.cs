﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Goat.Net.Models;
using Goat.Net.Models.Enums;

namespace Goat.Net.Clients
{
	/// <summary>
	/// A class to provide methods to interact with the <code>orders</code> section of the GOAT API
	/// </summary>
	public class OrdersClient : BaseClient
	{
		internal OrdersClient()
		{ }

		internal OrdersClient(string authToken) : base(authToken)
		{ }
		
		/// <summary>
		/// Gets a list of the user's orders
		/// </summary>
		/// <param name="orderType">The type of order to return</param>
		/// <param name="page">THe page of results to return</param>
		/// <returns>A list of <see cref="Order"/> objects</returns>
		public async Task<List<Order>> GetOrdersAsync(OrderType orderType, string page)
		{
			var requestUri = new Uri(Constants.OrdersUri, $"?filter={orderType.GetStringValue()}&page={page}");

			return await SendAuthenticatedRequestAsync<List<Order>>(requestUri, HttpMethod.Get);
		}

		/// <summary>
		/// Get a specific order
		/// </summary>
		/// <param name="orderId">The ID of the order</param>
		/// <returns>An <see cref="Order"/> object for the ID requested</returns>
		public async Task<Order> GetOrderAsync(string orderId)
		{
			var requestUri = new Uri(Constants.OrdersUri, orderId);

			return await SendAuthenticatedRequestAsync<Order>(requestUri, HttpMethod.Get);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderId"></param>
		/// <returns></returns>
		public async Task<List<Progress>> GetOrderProgressAsync(string orderId)
		{
			var requestUri = new Uri(Constants.OrdersUri, $"{orderId}/progress");

			return await SendAuthenticatedRequestAsync<List<Progress>>(requestUri, HttpMethod.Get);
		}

		/// <summary>
		/// Update the status of an order
		/// </summary>
		/// <param name="orderId">The ID of the order to update</param>
		/// <param name="orderStatus">The new status for the order</param>
		/// <returns>The updated <see cref="Order"/> object</returns>
		public async Task<Order> UpdateOrderStatusAsync(string orderId, OrderStatus orderStatus)
		{
			var requestUri = new Uri(Constants.OrdersUri, $"{orderId}/update_status");
			var content = new FormUrlEncodedContent(new Dictionary<string, string>
			{
				{"order[statusAction]", orderStatus.GetStringValue()}
			});

			return await SendAuthenticatedRequestAsync<Order>(requestUri, HttpMethod.Put, content);
		}
	}
}