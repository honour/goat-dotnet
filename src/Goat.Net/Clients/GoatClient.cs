﻿using Goat.Net.Models;

namespace Goat.Net.Clients
{
	/// <summary>
	/// The main class of the library. Provides access to each of the sub-clients
	/// </summary>
	public class GoatClient
	{
		/// <summary>
		/// The currencies sub-client
		/// </summary>
		public CurrenciesClient Currencies { get; }

		/// <summary>
		/// The orders sub-client
		/// </summary>
		public OrdersClient Orders { get; }

		/// <summary>
		/// The products sub-client
		/// </summary>
		public ProductsClient Products { get; }

		/// <summary>
		/// The product templates sub-client
		/// </summary>
		public ProductTemplatesClient ProductTemplates { get; }

		/// <summary>
		/// The region preferences sub-client
		/// </summary>
		public RegionPreferencesClient RegionPreferences { get; }

		/// <summary>
		/// The users sub-client
		/// </summary>
		public UsersClient Users { get; }

		/// <summary>
		/// The currently logged-in user
		/// </summary>
		public User LoggedInUser { get; set; }

		/// <summary>
		/// The default constructor for the GOAT Client
		/// </summary>
		public GoatClient()
		{
			Currencies = new CurrenciesClient();
			Orders = new OrdersClient();
			Products = new ProductsClient(this);
			ProductTemplates = new ProductTemplatesClient();
			RegionPreferences = new RegionPreferencesClient();
			Users = new UsersClient(this);
		}

		/// <summary>
		/// Alternative constructor which takes an auth token on initialisation
		/// <remarks>Can be used if the user's access token is already known, and will prevent logging in unnecessarily</remarks>
		/// </summary>
		/// <param name="authToken">The auth token to use</param>
		public GoatClient(string authToken)
		{
			Currencies = new CurrenciesClient(authToken);
			Orders = new OrdersClient(authToken);
			Products = new ProductsClient(this, authToken);
			ProductTemplates = new ProductTemplatesClient(authToken);
			RegionPreferences = new RegionPreferencesClient(authToken);
			Users = new UsersClient(this, authToken);

			LoggedInUser = Users.GetCurrentUserAsync().GetAwaiter().GetResult();
		}

		/// <summary>
		/// Sets the auth token to be used across all sub-clients
		/// <remarks>Can be used to change which user is being accessed</remarks>
		/// </summary>
		/// <param name="authToken">The auth token to use</param>
		public void SetAuthToken(string authToken)
		{
			Currencies.SetAuthToken(authToken);
			Orders.SetAuthToken(authToken);
			Products.SetAuthToken(authToken);
			ProductTemplates.SetAuthToken(authToken);
			RegionPreferences.SetAuthToken(authToken);
			Users.SetAuthToken(authToken);
		}
	}
}