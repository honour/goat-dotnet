﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Goat.Net.Models;

namespace Goat.Net.Clients
{
	/// <summary>
	/// A class to provide methods to interact with the <code>region_preferences</code> section of the GOAT API
	/// </summary>
	public class RegionPreferencesClient : BaseClient
	{
		internal RegionPreferencesClient()
		{ }
		
		internal RegionPreferencesClient(string authToken) : base(authToken)
		{ }
		
		/// <summary>
		/// Get's the region which is recommended for the current user
		/// </summary>
		/// <returns>A <see cref="Region"/> object for the recommended region</returns>
		public async Task<Region> GetRecommendedRegionAsync()
		{
			var requestUri = new Uri(Constants.RegionPreferencesUri, "recommended");
			
			return await SendAuthenticatedRequestAsync<Region>(requestUri, HttpMethod.Get);
		}
	}
}