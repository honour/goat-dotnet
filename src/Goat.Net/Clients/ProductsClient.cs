﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Goat.Net.Models;
using Goat.Net.Models.Enums;
using Goat.Net.Requests;
using Goat.Net.Responses;
using Newtonsoft.Json;

namespace Goat.Net.Clients
{
	/// <summary>
	/// A class to provide methods to interact with the <code>products</code> section of the GOAT API
	/// </summary>
	public class ProductsClient : BaseClient
	{
		private readonly GoatClient _goatClient;

		internal ProductsClient(GoatClient goatClient)
		{
			_goatClient = goatClient;
		}

		internal ProductsClient(GoatClient goatClient, string authToken) : base(authToken)
		{
			_goatClient = goatClient;
		}

		/// <summary>
		/// Gets a list of the user's products
		/// </summary>
		/// <param name="productType">The type of product to return</param>
		/// <param name="page">THe page of results to return</param>
		/// <returns>A list of <see cref="Product"/> objects</returns>
		public async Task<List<Product>> GetUserProductsAsync(ProductType productType, string page)
		{
			var requestUri = productType == ProductType.Pending
				? new Uri(Constants.UsersUri, $"{_goatClient.LoggedInUser.Slug}/pending?page={page}")
				: new Uri(Constants.ProductsUri, $"?filter={productType.GetStringValue()}&page={page}");

			return (await SendAuthenticatedRequestAsync<ProductsResponse>(requestUri, HttpMethod.Get)).Products;
		}

		/// <summary>
		/// Begin the listing process for a product on GOAT
		/// </summary>
		/// <param name="productListRequest">A <see cref="ProductListRequest"/> object containing the listing data</param>
		/// <returns>The newly created <see cref="Product"/> object</returns>
		public async Task<Product> ListProductAsync(ProductListRequest productListRequest)
		{
			var requestUri = new Uri(Constants.UsersUri, "me");

			var user = await SendAuthenticatedRequestAsync<User>(requestUri, HttpMethod.Get);

			requestUri = Constants.ProductsUri;
			var content = new FormUrlEncodedContent(new Dictionary<string, string>
			{
				{"product[userId]", user.Id.ToString()},
				{"product[saleStatus]", "pending"},
				{"product[priceCents]", productListRequest.PriceCents.ToString()},
				{"product[productTemplateId]", productListRequest.ProductTemplateId.ToString()},
				{"product[sizeUnit]", productListRequest.SizeUnit},
				{"product[size]", productListRequest.Size},
				{"product[boxCondition]", productListRequest.BoxCondition.GetStringValue()},
				{"product[shoeCondition]", productListRequest.ShoeCondition.GetStringValue()},
				{"product[hasDiscoloration]", ConvertBool(productListRequest.HasDiscoloration)},
				{"product[hasDefects]", ConvertBool(productListRequest.HasDefects)},
				{"product[hasScuffs]", ConvertBool(productListRequest.HasScuffs)},
				{"product[hasTears]", ConvertBool(productListRequest.HasTears)},
				{"product[hasMissingInsoles]", ConvertBool(productListRequest.HasMissingInsoles)},
				{"product[missingItems]", productListRequest.MissingItems}
			});

			return await SendAuthenticatedRequestAsync<Product>(requestUri, HttpMethod.Post, content);
		}

		/// <summary>
		/// Gets the data for a specific product
		/// </summary>
		/// <param name="productId">The ID of the product to retrieve</param>
		/// <returns>The associated <see cref="Product"/> object</returns>
		public async Task<Product> GetProductAsync(string productId)
		{
			var requestUri = new Uri(Constants.ProductsUri, $"{productId}");

			return await SendAuthenticatedRequestAsync<Product>(requestUri, HttpMethod.Get);
		}

		/// <summary>
		/// Get the pictures requested for this product listing
		/// </summary>
		/// <param name="productId">The ID of the product to obtain data for</param>
		/// <returns>A <see cref="RequestedPicturesResponse"/> object containing the details of which pictures are needed</returns>
		public async Task<RequestedPicturesResponse> GetRequestedPicturesAsync(string productId)
		{
			var requestUri = new Uri(Constants.ProductsUri, $"{productId}/requested_pictures");

			return await SendAuthenticatedRequestAsync<RequestedPicturesResponse>(requestUri, HttpMethod.Get);
		}

		/// <summary>
		/// Upload an image to a product
		/// </summary>
		/// <param name="productId">The ID of the product to upload the image to</param>
		/// <param name="imageType">The type of picture to upload. See <see cref="GetRequestedPicturesAsync"/> for getting a list of which image types are needed</param>
		/// <param name="imageData">The image to upload as a byte array</param>
		/// <returns>The updated <see cref="Product"/> object</returns>
		public async Task<Product> UploadProductImageAsync(string productId, ImageType imageType, byte[] imageData)
		{
			var requestUri = new Uri(Constants.ProductsUri, $"{productId}");
			var content =
				new StringContent(
					$"{HttpUtility.UrlEncode(imageType.GetStringValue())}={HttpUtility.UrlEncode(Convert.ToBase64String(imageData))}",
					Encoding.UTF8, "application/x-www-form-urlencoded");

			var httpClient = new HttpClient(new HttpClientHandler
			{
				CookieContainer = CookieContainer
			});
			httpClient.DefaultRequestHeaders.Add("User-Agent", "GOAT/2.34.0 (iPhone; iOS 14.0; Scale/2.00) Locale/en");
			httpClient.DefaultRequestHeaders.Add("Authorization", $"Token token=\"{AuthToken}\"");

			var response = await httpClient.PutAsync(requestUri, content);
			return JsonConvert.DeserializeObject<Product>(await response.Content.ReadAsStringAsync());
		}

		/// <summary>
		/// Put a listing live
		/// </summary>
		/// <param name="productId">The ID of the product to list</param>
		/// <returns>The updated <see cref="Product"/> object</returns>
		public async Task<Product> SubmitListingAsync(string productId)
		{
			var requestUri = new Uri(Constants.ProductsUri, $"{productId}/review");

			return await SendAuthenticatedRequestAsync<Product>(requestUri, HttpMethod.Post);
		}

		/// <summary>
		/// Put a listing into an <c>inactive</c> state
		/// </summary>
		/// <param name="productId">The ID of the product listing to deactivate</param>
		/// <returns>The updated <see cref="Product"/> object</returns>
		public async Task<Product> DeactivateListingAsync(string productId)
		{
			var requestUri = new Uri(Constants.ProductsUri, $"{productId}/deactivate");

			return await SendAuthenticatedRequestAsync<Product>(requestUri, HttpMethod.Post);
		}

		/// <summary>
		/// Delete a listing
		/// </summary>
		/// <param name="productId">The ID of the product listing to delete</param>
		/// <returns>The updated <see cref="Product"/> object</returns>
		public async Task<Product> DeleteListingAsync(string productId)
		{
			var requestUri = new Uri(Constants.ProductsUri, $"{productId}/cancel");

			return await SendAuthenticatedRequestAsync<Product>(requestUri, HttpMethod.Post);
		}
	}
}