﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Goat.Net.Responses;

namespace Goat.Net.Clients
{
	/// <summary>
	/// A class to provide methods to interact with the <code>currencies</code> section of the GOAT API
	/// </summary>
	public class CurrenciesClient : BaseClient
	{
		internal CurrenciesClient()
		{ }
		
		internal CurrenciesClient(string authToken) : base(authToken)
		{ }
		
		/// <summary>
		/// Get a list of currencies and their associated exchange rates
		/// </summary>
		/// <returns>A <see cref="CurrenciesResponse"/> object containing the currency data</returns>
		public async Task<CurrenciesResponse> GetCurrenciesAsync()
		{
			var requestUri = Constants.CurrenciesUri;
			
			return await SendAuthenticatedRequestAsync<CurrenciesResponse>(requestUri, HttpMethod.Get);
		}

		/// <summary>
		/// Sets the user's preferred currency
		/// </summary>
		/// <param name="isoCode">The ISO currency code</param>
		/// <returns>The success result of the request</returns>
		public async Task<bool> SetPreferredCurrencyAsync(string isoCode)
		{
			var requestUri = new Uri(Constants.CurrenciesUri, "set_preferred_currency");
			var content = new FormUrlEncodedContent(new Dictionary<string, string>
			{
				{"currency", isoCode}
			});
			
			var successResponse = await SendAuthenticatedRequestAsync<SuccessResponse>(requestUri, HttpMethod.Put, content);
			
			return successResponse.Success;
		}
	}
}