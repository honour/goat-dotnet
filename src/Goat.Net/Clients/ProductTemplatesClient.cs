﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Goat.Net.Models;

namespace Goat.Net.Clients
{
	/// <summary>
	/// A class to provide methods to interact with the <code>product_templates</code> section of the GOAT API
	/// </summary>
	public class ProductTemplatesClient : BaseClient
	{
		internal ProductTemplatesClient()
		{ }
		
		internal ProductTemplatesClient(string authToken) : base(authToken)
		{ }
		
		/// <summary>
		/// Gets the offer data for a specific size of a product
		/// </summary>
		/// <param name="productTemplateId">The ID or slug of the product template</param>
		/// <param name="size">The US shoe size data is required for</param>
		/// <returns>The offer data as a <see cref="OfferData"/> object</returns>
		public async Task<OfferData> GetOfferDataAsync(string productTemplateId, string size)
		{
			var requestUri = new Uri(Constants.ProductTemplatesUri, $"{productTemplateId}/offer_data_v2?size={size}");
			
			return await SendAuthenticatedRequestAsync<OfferData>(requestUri, HttpMethod.Get);
		}
		
		/// <summary>
		/// Get a <see cref="ProductTemplate"/> from its ID or slug
		/// </summary>
		/// <param name="productTemplateId">The ID or slug of the product template</param>
		/// <returns>The product template as a <see cref="ProductTemplate"/> object</returns>
		public async Task<ProductTemplate> GetProductTemplateAsync(string productTemplateId)
		{
			var requestUri = new Uri(Constants.ProductTemplatesUri, $"{productTemplateId}/show_v2");
			
			return await SendAuthenticatedRequestAsync<ProductTemplate>(requestUri, HttpMethod.Get);
		}
		
		/// <summary>
		/// Gets the listing prices for a product template
		/// </summary>
		/// <param name="productTemplateId">The ID or slug of the product template</param>
		/// <returns>A <see cref="ListingPrices"/> object for the product template requested</returns>
		public async Task<ListingPrices> GetListingPricesAsync(string productTemplateId)
		{
			var requestUri = new Uri(Constants.ProductsUri, $"listing_prices?productTemplateId={productTemplateId}");

			return await SendAuthenticatedRequestAsync<ListingPrices>(requestUri, HttpMethod.Get);
		}
	}
}