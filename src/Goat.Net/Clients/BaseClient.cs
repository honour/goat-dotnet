﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Goat.Net.Clients
{
	/// <summary>
	/// The base class for a sub-client. Contains methods required to interact with the GOAT API
	/// </summary>
	public abstract class BaseClient
	{
		private readonly HttpClient _httpClient;
		internal string AuthToken { get; private set; }
		internal static CookieContainer CookieContainer => new CookieContainer();

		internal BaseClient()
		{
			_httpClient = new HttpClient(new HttpClientHandler
			{
				CookieContainer = new CookieContainer(),
			});
		}
		
		internal BaseClient(string authToken)
		{
			_httpClient = new HttpClient(new HttpClientHandler
			{
				CookieContainer = CookieContainer,
			});
			AuthToken = authToken;
		}

		internal void SetAuthToken(string authToken)
		{
			AuthToken = authToken;
		}

		internal async Task<T> SendUnauthenticatedRequestAsync<T>(Uri requestUri, HttpMethod httpMethod,
			HttpContent httpContent = null)
		{
			return await SendRequestAsync<T>(requestUri, httpMethod, httpContent);
		}

		internal async Task<T> SendAuthenticatedRequestAsync<T>(Uri requestUri, HttpMethod httpMethod,
			HttpContent httpContent = null)
		{
			return await SendRequestAsync<T>(requestUri, httpMethod, httpContent, true);
		}

		private async Task<T> SendRequestAsync<T>(Uri requestUri, HttpMethod httpMethod, HttpContent httpContent = null,
			bool authenticated = false)
		{
			var request = new HttpRequestMessage
			{
				RequestUri = requestUri,
				Method = httpMethod,
				Content = httpContent,
				Headers =
				{
					{"User-Agent", "Goat.Net for Swift (7.0.5) iOS (14.0)"}
				}
			};

			if (authenticated)
				request.Headers.Add("Authorization", $"Token token=\"{AuthToken}\"");

			var response = await _httpClient.SendAsync(request);
			return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
		}

		internal static string ConvertBool(bool b)
		{
			return b ? "1" : "0";
		}
	}
}