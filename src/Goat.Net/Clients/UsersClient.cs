﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Goat.Net.Models;
using Goat.Net.Responses;

namespace Goat.Net.Clients
{
	/// <summary>
	/// A class to provide methods to interact with the <code>users</code> section of the GOAT API
	/// </summary>
	public class UsersClient : BaseClient
	{
		private readonly GoatClient _goatClient;

		internal UsersClient(GoatClient goatClient)
		{
			_goatClient = goatClient;
		}

		internal UsersClient(GoatClient goatClient, string authToken) : base(authToken)
		{
			_goatClient = goatClient;
		}
		
		/// <summary>
		/// Sign into GOAT using an email and password
		/// </summary>
		/// <param name="email">The user's email</param>
		/// <param name="password">The user's password</param>
		/// <returns>A <see cref="User"/> object associated with the email specified</returns>
		public async Task<User> SignInAsync(string email, string password)
		{
			var requestUri = new Uri(Constants.UsersUri, "sign_in");
			var content = new FormUrlEncodedContent(new Dictionary<string, string>
			{
				{"user[login]", email},
				{"user[password]", password}
			});
			
			var user = await SendUnauthenticatedRequestAsync<User>(requestUri, HttpMethod.Post, content);
			
			_goatClient.LoggedInUser = user;

			return user;
		}

		/// <summary>
		/// Sign out of GOAT if logged in
		/// </summary>
		/// <returns>The success result of the request</returns>
		public async Task<bool> SignOutAsync()
		{
			var requestUri = new Uri(Constants.UsersUri, "sign_out");

			var successResponse = await SendAuthenticatedRequestAsync<SuccessResponse>(requestUri, HttpMethod.Delete);

			return successResponse.Success;
		}

		/// <summary>
		/// Get the current GOAT user if logged in
		/// </summary>
		/// <returns>A <see cref="User"/> object for the logged-in user</returns>
		public async Task<User> GetCurrentUserAsync()
		{
			var requestUri = new Uri(Constants.UsersUri, "me");
			
			var user = await SendAuthenticatedRequestAsync<User>(requestUri, HttpMethod.Get);

			_goatClient.LoggedInUser = user;
			
			return user;
		}

		/// <summary>
		/// Sets the user's vacation status
		/// </summary>
		/// <param name="onVacation">Whether or not the user is on vacation</param>
		/// <returns>The updated <see cref="User"/> object.</returns>
		public async Task<User> SetVacationModeAsync(bool onVacation)
		{
			var requestUri = new Uri(Constants.UsersUri, "pause_products");
			var content = new FormUrlEncodedContent(new Dictionary<string, string>
			{
				{"pause", ConvertBool(onVacation)}
			});
			
			var user = await SendAuthenticatedRequestAsync<User>(requestUri, HttpMethod.Post, content);
			
			_goatClient.LoggedInUser = user;
			
			return user;
		}

		/// <summary>
		/// Get the user's buyer counts
		/// </summary>
		/// <returns>A <see cref="BuyerCounts"/> object containing this information</returns>
		public async Task<BuyerCounts> GetBuyerCountsAsync()
		{
			var requestUri = new Uri(Constants.UsersUri, "buyer_counts");
			
			return await SendAuthenticatedRequestAsync<BuyerCounts>(requestUri, HttpMethod.Get);
		}
		
		/// <summary>
		/// Get the user's seller counts
		/// </summary>
		/// <returns>A <see cref="SellerCounts"/> object containing this information</returns>
		public async Task<SellerCounts> GetSellerCountsAsync()
		{
			var requestUri = new Uri(Constants.UsersUri, "seller_counts");
			
			return await SendAuthenticatedRequestAsync<SellerCounts>(requestUri, HttpMethod.Get);
		}
		
		/// <summary>
		/// Get the user's cash out information
		/// </summary>
		/// <returns>A <see cref="CashOut"/> object with the requested data</returns>
		public async Task<CashOut> GetCashOutAsync()
		{
			var requestUri = new Uri(Constants.UsersUri, "cash_out");
			
			return await SendAuthenticatedRequestAsync<CashOut>(requestUri, HttpMethod.Get);
		}
	}
}